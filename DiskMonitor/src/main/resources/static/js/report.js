/**
 * 
 */
var surl;
var email;
var aktivacnyKod;
var popup;
var checkedFileOwners;

$.ajaxSetup({
	cache : false,
	contentType : 'charset=utf-8',
});

$(document).ajaxComplete(function() {
	$('body').css('cursor', 'auto');
});

$(document).ajaxSend(function() {
	$('body').css('cursor', 'wait');
});

$(document).ready(function () {
	
	var protocol = window.location.protocol;
	var host = window.location.host;
	surl = protocol + '//' + host + '/DiskMonitor/checked-folder';

	$('#checkedFolderTable').DataTable({});
	$('#ownerSpaceUsageTable').DataTable({});
	
	// Selection of single row only
	$('#checkedFolderTable tbody').on('click','tr',function(){
		if ( $(this).hasClass('bg-info') ) {
            $(this).removeClass('bg-info');
        }
        else {
        	$('#checkedFolderTable').DataTable().$('tr.bg-info').removeClass('bg-info');
            $(this).addClass('bg-info');
            getCheckedFilesByOwner($('#checkedFolderTable').dataTable().fnGetData(this).id);
        }
	});
	
	// Add event listener for opening and closing details
    $('#ownerSpaceUsageTable tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = $('#ownerSpaceUsageTable').DataTable().row( tr );
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.remove();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child(nestedTableFormat(row.data())).show();
            tr.addClass('shown');
        }
    } );
		
	getCheckedFolders();
	
});

// Ajax call for all checks on folder
function getCheckedFolders(){
	$.ajax({
		url: surl,
		type: 'get',
		success: function(result){
			for (var i = 0; i < result.length; i++) {
				var d = new Date(result[i].date);
				var formattedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear();
				var hours = (d.getHours() < 10) ? "0" + d.getHours() : d.getHours();
				var minutes = (d.getMinutes() < 10) ? "0" + d.getMinutes() : d.getMinutes();
				var formattedTime = hours + ":" + minutes;

				result[i].date = formattedDate + " " + formattedTime;
				result[i].size = result[i].size / 1024 / 1024;
			}
			$('#checkedFolderTable').DataTable({
				autoWidth: false,
				destroy : true,
				data : result,
				columns: [{
					visible: false,
					searchable: false,
					data: 'id'
				},{
					data: 'date'
				},{
					data: 'path'
				},{
					data: 'size',
					render: $.fn.dataTable.render.number( ' ', '.', 2 )
				}]
			});
		}
	});
}

// Ajax call for all checked folders for selected folder check
function getCheckedFilesByOwner(id){
	checkedFileOwners = [];
	$.ajax({
		url: surl + "/" + id,
		type: 'get',
		success: function(result){
			// transform list of files into list of owners
			for (var i = 0; i < result.checkedFiles.length; i++) {
				addCheckedFileOwner(result.checkedFiles[i]);
			}
			for (var i = 0; i < checkedFileOwners.length; i++){
				checkedFileOwners[i].size = checkedFileOwners[i].size / 1024 / 1024;
			}
			$('#ownerSpaceUsageTable').DataTable({
				autoWidth: false,
				destroy : true,
				order: [[1, 'asc']],
				data : checkedFileOwners,
				columns: [{
					className: 'details-control',
					orderable: false,
					data: null,
					defaultContent: ''
				},{
					data: 'owner'
				},{
					data: 'fileCount'
				},{
					data: 'size',
					render: $.fn.dataTable.render.number( ' ', '.', 2 )
				}]
			});
			$('#ownerSpaceUsageTableWrapper').show();
		}
	});
}

// Count the total count and size of files based on owner
function addCheckedFileOwner(checkedFile){
	for (var i=0; i < checkedFileOwners.length; i++) {
        if (checkedFileOwners[i].owner === checkedFile.owner) {
        	checkedFileOwners[i].fileCount = checkedFileOwners[i].fileCount + 1;
        	checkedFileOwners[i].size = checkedFileOwners[i].size + checkedFile.size;
        	var file = {'path': checkedFile.path, 'size': checkedFile.size / 1024 }
        	checkedFileOwners[i].files.push(file);
            return;
        }
    }
	var obj = {'owner': checkedFile.owner, 'fileCount': 1, 'size': checkedFile.size, 'files': [{'path': checkedFile.path, 'size': checkedFile.size / 1024 }]}
	checkedFileOwners.push(obj);
	return;
}

function nestedTableFormat ( data ) {
    // `d` is the original data object for the row
    var nestedTable = '<table cellpadding="5" cellspacing="0" border="0"' 
    	+ 'class="table-sm table-striped table-bordered" style="padding-left:50px;"><thead class="thead-dark">'
    	+ '<tr><th>Súbor</th><th>Veľkosť (kB)</th></tr></thead><tbody>';

    var files = data.files.sort(function(a, b){return b.size - a.size});
    for (var i=0; i < files.length; i++){
    	nestedTable = nestedTable + '<tr>' + '<td>'+files[i].path+'</td>' + '<td>'+(files[i].size).toLocaleString('fr', {minimumFractionDigits: 2, maximumFractionDigits: 2})+'</td>' + '</tr>';
    }
    nestedTable = nestedTable + '</tbody></table>';
    return nestedTable;
}