package sk.hlavenka.diskmonitor;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileOwnerAttributeView;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import sk.hlavenka.diskmonitor.config.AppConfiguration;
import sk.hlavenka.diskmonitor.domain.CheckedFile;
import sk.hlavenka.diskmonitor.domain.CheckedFolder;
import sk.hlavenka.diskmonitor.domain.EmailTemplate;
import sk.hlavenka.diskmonitor.repository.CheckedFileRepository;
import sk.hlavenka.diskmonitor.repository.CheckedFolderRepository;
import sk.hlavenka.diskmonitor.service.EmailService;

@Component
public class ScheduledTasks {

    private static final Logger log = LoggerFactory.getLogger(ScheduledTasks.class);

    private final AppConfiguration appConfiguration;
    private final EmailService emailService;
    private final CheckedFolderRepository checkedFolderRepository;
    private final CheckedFileRepository checkedFileRepository;
    
    public ScheduledTasks(AppConfiguration appConfiguration,EmailService emailService, CheckedFolderRepository checkedFolderRepository, CheckedFileRepository checkedFileRepository) { 
    	this.appConfiguration = appConfiguration;
    	this.emailService = emailService;
    	this.checkedFolderRepository = checkedFolderRepository;
    	this.checkedFileRepository = checkedFileRepository;
    };
    
    @Scheduled(cron= "${app.cron-expression}")
    public void checkSpace() {
        log.info("Starting scheduled task checkSpace.");
        
        try {
        	File disk = new File(appConfiguration.getDisk());
        	int diskSpaceUsage = (int)(100.0 -((disk.getUsableSpace()*100.0)/disk.getTotalSpace()));
        
        	log.info("Disk '" + appConfiguration.getDisk() + "' space usage is at " + diskSpaceUsage + "%.");

        	if (diskSpaceUsage >= appConfiguration.getDiskSpaceThreshold()) {
        		sendEmail(diskSpaceUsage, disk.getUsableSpace());
        	}
        
        	CheckedFolder checkedFolder = new CheckedFolder(new Timestamp(System.currentTimeMillis()), appConfiguration.getFolder(), 0L);
    	
        	checkedFolder = folderWalk(checkedFolder, Paths.get(appConfiguration.getFolder()));
    	
        	checkedFolderRepository.save(checkedFolder);
        	for (CheckedFile checkedFile : checkedFolder.getCheckedFiles()) {
        		checkedFile.setCheckedFolderId(checkedFolder.getId());
				checkedFileRepository.save(checkedFile);
			}
    	
        	log.info("Folder '" + appConfiguration.getFolder() + "' size is " + checkedFolder.getSize() /1024 /1024 + " MB");
        	log.info("Finished scheduled task checkSpace.");
        } catch (IOException e) {
			log.error("Failed scheduled task checkSpace. (" + e + ")");
			e.printStackTrace();
		}
    }
    
    private void sendEmail(int diskSpaceUsage, long usableSpace) {
    	try {
    		log.info("Sending notification email.");
    		
			Map<String,String> replacements = new HashMap<String,String>();
			replacements.put("disk", appConfiguration.getDisk());
			replacements.put("percentage", Integer.toString(diskSpaceUsage));
			replacements.put("usableSpace", Long.toString(usableSpace /1024 /1024));

			EmailTemplate template = new EmailTemplate(appConfiguration.getEmailTemplateId());
			String msgText = template.getTemplate(replacements);
			
			emailService.send(appConfiguration.getEmailTo(), appConfiguration.getEmailSubject(), msgText);
			
			log.info("Notification email was successfully sent.");
		} catch (IOException e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}
    }
    
    public static CheckedFolder folderWalk(CheckedFolder checkedFolder, Path path) throws IOException {
    	
    	final AtomicLong size = new AtomicLong(0);
    	
    	List<CheckedFile> checkedFiles = new ArrayList<CheckedFile>();

    	try {
            Files.walkFileTree(path, new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {

                    size.addAndGet(attrs.size());
                    FileOwnerAttributeView ownerAttributeView = Files.getFileAttributeView(path, FileOwnerAttributeView.class);
                    checkedFiles.add(new CheckedFile(file.toString(), ownerAttributeView.getOwner().getName(), attrs.size()));
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult visitFileFailed(Path file, IOException e) {

                    log.error("folderWalk skipped: " + file + " (" + e + ")");
                    // Skip folders that can't be traversed
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult postVisitDirectory(Path dir, IOException e) {

                    if (e != null)
                    	log.error("folderWalk had trouble traversing: " + dir + " (" + e + ")");
                    // Ignore errors traversing a folder
                    return FileVisitResult.CONTINUE;
                }
            });
        } catch (IOException e) {
        	log.error("folderWalk failed to read from specified folder " + path.toString() + " (" + e + ")");
            throw e;
        }

    	checkedFolder.setSize(size.get());
    	checkedFolder.setCheckedFiles(checkedFiles);
    	
        return checkedFolder;
    }
}