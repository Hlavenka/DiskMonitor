package sk.hlavenka.diskmonitor.domain;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

@Entity
public class CheckedFolder implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2077356409900252401L;
	
	private Long id;
	private Timestamp date;
	private String path;
	private Long size;
	private List<CheckedFile> checkedFiles;
		
	protected CheckedFolder() {}
	
	public CheckedFolder(Timestamp date, String path, Long size) {
		this.date = date;
		this.path = path;
		this.size = size;
	}

	@Id
	@SequenceGenerator(name="checked_folder_sequence_generator",sequenceName="seq_checked_folder",allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="checked_folder_sequence_generator")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getDate() {
		return date;
	}

	public void setDate(Timestamp date) {
		this.date = date;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public Long getSize() {
		return size;
	}

	public void setSize(Long size) {
		this.size = size;
	}

	@Transient
	public List<CheckedFile> getCheckedFiles() {
		return checkedFiles;
	}

	public void setCheckedFiles(List<CheckedFile> checkedFiles) {
		this.checkedFiles = checkedFiles;
	}
}
