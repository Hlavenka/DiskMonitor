package sk.hlavenka.diskmonitor.domain;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Map;

public class EmailTemplate {

	private String template;
	
	public EmailTemplate(String templateId) throws IOException {
		this.template = loadTemplate(templateId);
	}
	
	private String loadTemplate(String templateId) throws IOException {
		File file = new File(getClass().getClassLoader().getResource("email-templates/"+templateId).getFile());
		String content = "";
		try {
			content = new String(Files.readAllBytes(file.toPath()));
		} catch (IOException e) {
			throw new IOException("Could not read template with ID = " + templateId);
		}
		return content;
	}
	
	public String getTemplate(Map<String,String> replacements) {
		String cTemplate = this.getTemplate();
		for (Map.Entry<String, String> entry : replacements.entrySet()) {
			cTemplate = cTemplate.replace("{{" + entry.getKey() + "}}", entry.getValue());
		}
		return cTemplate;
	}
	
	public String getTemplate() {
		return template;
	}
}
