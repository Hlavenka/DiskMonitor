package sk.hlavenka.diskmonitor.domain.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class CheckedFileNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6023244870041935051L;

	public CheckedFileNotFoundException(String message) {
		super(message);
	}

}
