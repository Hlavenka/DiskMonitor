package sk.hlavenka.diskmonitor.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity
public class CheckedFile implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1099927833538857144L;
	
	private Long id;
	private Long checkedFolderId;
	private String path;
	private String owner;
	private Long size;
	
	protected CheckedFile() {}
	
	public CheckedFile(String path, String owner, Long size) {
		this.path = path;
		this.owner = owner;
		this.size = size;
	}

	@Id
	@SequenceGenerator(name="checked_file_sequence_generator",sequenceName="seq_checked_file",allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="checked_file_sequence_generator")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCheckedFolderId() {
		return checkedFolderId;
	}

	public void setCheckedFolderId(Long checkedFolderId) {
		this.checkedFolderId = checkedFolderId;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public Long getSize() {
		return size;
	}

	public void setSize(Long size) {
		this.size = size;
	}

}
