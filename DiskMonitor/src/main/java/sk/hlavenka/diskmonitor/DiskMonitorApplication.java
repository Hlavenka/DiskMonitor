package sk.hlavenka.diskmonitor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class DiskMonitorApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiskMonitorApplication.class, args);
	}
	
	
}
