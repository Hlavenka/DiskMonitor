package sk.hlavenka.diskmonitor.controller;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import sk.hlavenka.diskmonitor.domain.exception.CheckedFileNotFoundException;
import sk.hlavenka.diskmonitor.domain.exception.CheckedFolderNotFoundException;
import sk.hlavenka.diskmonitor.domain.CheckedFolder;
import sk.hlavenka.diskmonitor.repository.CheckedFileRepository;
import sk.hlavenka.diskmonitor.repository.CheckedFolderRepository;

@RestController
public class CheckedFolderController {

	private static final Logger log = LoggerFactory.getLogger(CheckedFolderController.class);
	
    private final CheckedFolderRepository checkedFolderRepository;
    private final CheckedFileRepository checkedFileRepository;
    
    public CheckedFolderController(CheckedFolderRepository checkedFolderRepository, CheckedFileRepository checkedFileRepository) { 
    	this.checkedFolderRepository = checkedFolderRepository;
    	this.checkedFileRepository = checkedFileRepository;
    };
    
    @RequestMapping(path = "/checked-folder", method = RequestMethod.GET)
	public List<CheckedFolder> checkedFolder(){
    	log.info("REST /checked-folder was called.");
    	
    	return checkedFolderRepository.findAll();
    }
    
	@RequestMapping(path = "/checked-folder/{id}", method = RequestMethod.GET)
	public CheckedFolder checkedFolder(@PathVariable("id") Long id) throws ParseException {
		log.info("REST /checked-folder/" + id + " was called.");
		
		try {
			CheckedFolder checkedFolder = checkedFolderRepository.findById(id)
										  .orElseThrow(() -> new CheckedFolderNotFoundException("CheckedFolder with id = " + id + " does not exist"));
			
			checkedFolder.setCheckedFiles(checkedFileRepository.findByCheckedFolderId(id)
															   .orElseThrow(() -> new CheckedFileNotFoundException("CheckedFile with reference to CheckedFolder.id = " + id + " does not exist")));
			return checkedFolder;
		} catch (CheckedFolderNotFoundException | CheckedFileNotFoundException e) {
			log.error(e.getMessage());
			throw e;
		}
	}
	
	@ExceptionHandler({CheckedFolderNotFoundException.class, CheckedFileNotFoundException.class})
	private void handleBadRequest(Exception e,HttpServletResponse response) throws IOException{
		response.sendError(HttpStatus.BAD_REQUEST.value(),e.getMessage());
	}
}
