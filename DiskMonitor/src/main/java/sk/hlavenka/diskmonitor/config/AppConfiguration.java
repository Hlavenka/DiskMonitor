package sk.hlavenka.diskmonitor.config;

import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

@Validated
@Component
@ConfigurationProperties("app")
public class AppConfiguration {

	@NotEmpty
	private String cronExpression;
	
	@Pattern(regexp = "[a-zA-Z]{1}\\:")
	private String disk;
	
	@Min(value = 0)
	@Max(value = 100)
	private int diskSpaceThreshold;
	
	@NotEmpty
	private String folder;
	
	@Email
	private String emailTo;
	
	@NotEmpty
	private String emailSubject;
	
	@NotEmpty
	private String emailTemplateId;
	
	
	public String getCronExpression() {
		return cronExpression;
	}
	
	public void setCronExpression(String cronExpression) {
		this.cronExpression = cronExpression;
	}
	
	public String getDisk() {
		return disk;
	}
	
	public void setDisk(String disk) {
		this.disk = disk.toUpperCase();
	}
	
	public int getDiskSpaceThreshold() {
		return diskSpaceThreshold;
	}
	
	public void setDiskSpaceThreshold(int diskSpaceThreshold) {
		this.diskSpaceThreshold = diskSpaceThreshold;
	}
	
	public String getFolder() {
		return folder;
	}
	
	public void setFolder(String folder) {
		this.folder = folder;
	}
	
	public String getEmailTo() {
		return emailTo;
	}
	
	public void setEmailTo(String emailTo) {
		this.emailTo = emailTo;
	}
	
	public String getEmailSubject() {
		return emailSubject;
	}
	
	public void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
	}
	
	public String getEmailTemplateId() {
		return emailTemplateId;
	}
	
	public void setEmailTemplateId(String emailTemplateId) {
		this.emailTemplateId = emailTemplateId;
	}
}
