package sk.hlavenka.diskmonitor.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sk.hlavenka.diskmonitor.domain.CheckedFolder;


@Repository
public interface CheckedFolderRepository extends JpaRepository<CheckedFolder, Long>{

	Optional<List<CheckedFolder>> findByDateGreaterThan(Date start);
	
	Optional<List<CheckedFolder>> findByDateLessThan(Date end);
	
	Optional<List<CheckedFolder>> findByDateBetween(Date start, Date end);
	
}
