package sk.hlavenka.diskmonitor.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sk.hlavenka.diskmonitor.domain.CheckedFile;

@Repository
public interface CheckedFileRepository extends JpaRepository<CheckedFile, Long>{
	
	Optional<List<CheckedFile>> findByCheckedFolderId(Long id);
	
	Optional<List<CheckedFile>> findByOwner(String owner);
	
}
