package sk.hlavenka.diskmonitor.service;

import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

@Component
public class EmailService {
	
	private final JavaMailSender emailSender;
	    
	public EmailService(JavaMailSender emailSender) { 
		this.emailSender = emailSender;
	};
	
	public void send(String to, String subject, String text) {
		
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(to);
		msg.setSubject(subject);
		msg.setText(text);

		emailSender.send(msg);
	}
}
