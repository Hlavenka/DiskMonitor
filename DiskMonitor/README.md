# Disk Monitor
Aplication for regular monitoring of available disk space on a drive and a specific folder.
The number and size of stored files by different users is monitored on the folder.

### Used technologies
- Maven
- Java 10
- Spring Boot starters
	- web 
		- RESTful services
		- Jetty servlet container
	- security (Spring Security)
	- data-jpa (Spring Data JPA with Hibernate)
	- mail (Java Mail and Spring Framework's email sending support)
	- thymeleaf
- H2 Embedded Database
- jQuery
- Bootstrap
- DataTables

### Application configuration
Configuration of the application is done by modifying **application.properties** file in the resources folder.

The server for sending emails is configured for Gmail.
```
spring.mail.host=smtp.gmail.com
spring.mail.port=587
spring.mail.username=diskmonitor.hlavenka
spring.mail.password=
spring.mail.properties.mail.smtp.auth=true
spring.mail.properties.mail.smtp.starttls.enable=true
spring.mail.properties.mail.smtp.starttls.required=true
```

Set the subject and recipient of the notification email.
```
app.email-to=to@gmail.com
app.email-subject=Disk presiahol hranicu ${app.disk-space-threshold}%
```
Specified file that will be used as a template for the e-mail body.
```
app.email-template-id=email-plain.txt
```
Specified drive and folder for monitoring.
```
app.disk=E:
app.folder=E:\\files
```
Threshold for the used space of a drive before sending notification email specified in percentage.
```
app.disk-space-threshold=80
```
Periodicity of monitoring.
```
app.cron-expression=00 00 22 * * ?
```

### Web client
Application is secured with user authentication and secure HTTP over TLS communication.
There is one user created for in memory authentication to all endpoints. 
```
user:		user
password:	password
```
For secured communication application uses self signed certificate.
To disable HTTPS and go to HTTP communication just delete those two properties.
```
server.ssl.key-store=classpath:keystore.jks
server.ssl.key-store-password=diskMonitorCertPassword
``` 
Once the application is running, the web client is accessible on this [link](https://localhost:8443/DiskMonitor/).
```
https://localhost:8443/DiskMonitor/
``` 